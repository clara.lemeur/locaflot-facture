package classes;

/**
 * @author Clara
 */
public class Louer {
	private int id_contrat;
	private String id_embarcation;
	private int nb_personnes;
	
	/**
	 * to get the identifiant of a contract
	 * @return int
	 */
	public int getId_contrat() {
		return id_contrat;
	}
	
	/**
	 * to modify the identifiant of a contract
	 * @param id_contrat
	 */
	public void setId_contrat(int id_contrat) {
		this.id_contrat = id_contrat;
	}
	
	/**
	 * to get the identifiant the boat
	 * @return String
	 */
	public String getId_embarcation() {
		return id_embarcation;
	}
	
	/**
	 * to modify the identifiant of the boat
	 * @param id_embarcation
	 */
	public void setId_embarcation(String id_embarcation) {
		this.id_embarcation = id_embarcation;
	}
	
	/**
	 * to get the number of people who rent the boat
	 * @return int
	 */
	public int getNb_personnes() {
		return nb_personnes;
	}
	
	/**
	 * to modify the number of people who rent the boat
	 * @param nb_personnes
	 */
	public void setNb_personnes(int nb_personnes) {
		this.nb_personnes = nb_personnes;
	}
	
	/**
	 * Default constructor
	 */
	public Louer() {
		this.id_contrat = 0;
		this.id_embarcation = null;
		this.nb_personnes = 0;
	}

	
	/**
	 * Constructor with parameter
	 * @param id_contrat
	 * @param id_embarcation
	 * @param nb_personnes
	 */
	public Louer(int id_contrat, String id_embarcation, int nb_personnes) {
		this.id_contrat = id_contrat;
		this.id_embarcation = id_embarcation;
		this.nb_personnes = nb_personnes;
	}

	/**
	 * Methode toString
	 */
	@Override
	public String toString() {
		return "Louer [id_contrat=" + id_contrat + ", id_embarcation=" + id_embarcation + ", nb_personnes=" + nb_personnes + "]";
	}
	
	
	
}
