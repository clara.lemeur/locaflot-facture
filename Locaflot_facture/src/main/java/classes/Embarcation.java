package classes;

/**
 * @author Clara Le Meur
 */
public class Embarcation {
	private String id;
	private String couleur;
	private boolean disponible;
	private String type;
	
	/**
	 * to get the identifiant of a boat
	 * @return
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * to modify the identifiant of a boat
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * to get the color of a boat
	 * @return
	 */
	public String getCouleur() {
		return couleur;
	}
	
	/**
	 * to modify the color of a boat
	 * @param couleur
	 */
	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}
	
	/**
	 * to know if the boat is available
	 * @return
	 */
	public boolean getDisponible() {
		return disponible;
	}
	
	/**
	 * to modify the condition of the boat (available/not available)
	 * @param disponible
	 */
	public void setDisponible(boolean disponible) {
		this.disponible = disponible;
	}
	
	/**
	 * to get the type of a boat
	 * @return
	 */
	public String getType() {
		return type;
	}
	
	/**
	 * to modify the type of a boat
	 * @param type
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	/**
	 * Default constructor
	 */
	public Embarcation() {
		this.id = null;
		this.couleur = null;
		this.disponible = true;
		this.type = null;
	}

	/**
	 * Constructor with parameter
	 * @param id
	 * @param couleur
	 * @param disponible
	 * @param type
	 */
	public Embarcation(String id, String couleur, boolean disponible, String type) {
		this.id = id;
		this.couleur = couleur;
		this.disponible = disponible;
		this.type = type;
	}

	/**
	 * Methode toString
	 */
	@Override
	public String toString() {
		return "Embarcation [id=" + id + ", couleur=" + couleur + ", disponible=" + disponible + ", type=" + type + "]";
	}
	
	public String InfoBoat() {
		return id + " - " + type ;
	}
}
