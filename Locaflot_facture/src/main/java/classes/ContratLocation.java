package classes;

/**
 * @author Clara Le Meur
 */
public class ContratLocation {
	private int id;
	private String date;
	private String heure_debut;
	private String heure_fin;
	
	/**
	 * to get the identifiant of a contract
	 * @return int
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * to modify the identifiant of a contract
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * to get the creation date of a contract
	 * @return String
	 */
	public String getDate() {
		return date;
	}
	
	/**
	 * to modify the creation date of a contract
	 * @param date
	 */
	public void setDate(String date) {
		this.date = date;
	}
	
	/**
	 * to get the start time of a contract
	 * @return String
	 */
	public String getHeure_debut() {
		return heure_debut;
	}
	
	/**
	 * to modify the start time of a contract
	 * @param heure_debut
	 */
	public void setHeure_debut(String heure_debut) {
		this.heure_debut = heure_debut;
	}
	
	/**
	 * to get the end time of a contract
	 * @return
	 */
	public String getHeure_fin() {
		return heure_fin;
	}
	
	/**
	 * to modify the end time of a contract
	 * @param heure_fin
	 */
	public void setHeure_fin(String heure_fin) {
		this.heure_fin = heure_fin;
	}
	
	/**
	 * Default constructor
	 */
	public ContratLocation() {
		this.id = 0;
		this.date = null;
		this.heure_debut = null;
		this.heure_fin = null;
	}

	/**
	 * Constructor with parameter
	 * @param id
	 * @param date
	 * @param heure_debut
	 * @param heure_fin
	 */
	public ContratLocation(int id, String date, String heure_debut, String heure_fin) {
		super();
		this.id = id;
		this.date = date;
		this.heure_debut = heure_debut;
		this.heure_fin = heure_fin;
	}

	/**
	 * Methode toString
	 */
	@Override
	public String toString() {
		return "Contrat_location [id=" + id + ", date=" + date + ", heure_debut=" + heure_debut + ", heure_fin="
				+ heure_fin + "]";
	}
	
	
	
	
}
