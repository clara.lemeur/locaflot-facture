package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import classes.ContratLocation;

public class ContratLocationDAO extends DAO<ContratLocation> {

	@Override
	public void create(ContratLocation obj) {
		try {
			PreparedStatement prepare = this.connect.prepareStatement("INSERT INTO \"locaflot_facture\".contrat_location VALUES(?, ?, ?, ?)");
			prepare.setInt(1, obj.getId());
			prepare.setString(2, obj.getDate());
			prepare.setString(3, obj.getHeure_debut());
			prepare.setString(4, obj.getHeure_fin());
			prepare.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public ContratLocation read(int id) {
		ContratLocation contract = new ContratLocation();
		try {
			ResultSet result = this.connect
				.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)
				.executeQuery("SELECT * FROM \"locaflot_facture\".contrat_location WHERE id = '" + id + "'");

			if (result.first())
				contract = new ContratLocation(id, result.getString("date"), result.getString("heure_debut"), result.getString("heure_fin"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return contract;
	}

	@Override
	public void update(ContratLocation obj) {
		try {
			this.connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE).executeUpdate(
					"UPDATE \"locaflot_facture\".contrat_location SET "
					+ "date='" + obj.getDate() + "', "
					+ "heure_debut='" + obj.getHeure_debut() + "', "
					+ "heure_fin= '" + obj.getHeure_fin() + "'" 
					+ " WHERE code = '" + obj.getId() + "'"
					);
			obj = this.read(obj.getId());
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void delete(ContratLocation obj) {
		try {
			this.connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)
					.executeUpdate("DELETE FROM \"locaflot_facture\".contrat_location WHERE code = '" + obj.getId() + "'");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<ContratLocation> recupAll() {
		List<ContratLocation> listeContrat = new ArrayList<ContratLocation>();

		// declaration de l'objet qui servira pour la requete SQL
		try {
			Statement requete = this.connect.createStatement();

			// definition de l'objet qui recupere le resultat de l'execution de la requete
			ResultSet curseur = requete.executeQuery("select * from \"locaflot_facture\".contrat_location");

			// tant qu'il y a une ligne "resultat" a  lire
			while (curseur.next()) {
				// objet pour la recup d'une ligne de la table Club
				ContratLocation Contrat = new ContratLocation();
				
				Contrat.setId(curseur.getInt("id"));
				Contrat.setDate(curseur.getString("date"));
				Contrat.setHeure_debut(curseur.getString("heure_debut"));
				Contrat.setHeure_fin(curseur.getString("heure_fin"));
				
				listeContrat.add(Contrat);
			}
			curseur.close();
			requete.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listeContrat;
	}

	
	
}
