package dao;

import java.sql.Connection;
import java.util.List;

public abstract class DAO<T> {
	/**
	 * connection object instance type attribute of the connection class
	 */
	public Connection connect = ConnectBdd.getInstance();

	/**
	 * abstract method for inserting a row in a table
	 * 
	 * @param obj
	 */
	public abstract void create(T obj);

	/**
	 * abstract method for displaying a row in a table
	 * 
	 * @param id
	 */
	public abstract T read(int id);

	/**
	 * abstract method for modifying a row in a table
	 * 
	 * @param obj
	 */
	public abstract void update(T obj);

	/**
	 * abstract method for deleting the row of a table
	 * 
	 * @param obj
	 */
	public abstract void delete(T obj);

	/**
	 * abstract method which returns a list of objects retrieving all the records of a table
	 */
	public abstract List<T> recupAll();

	//public abstract List<T> recupAll(int id);
}
