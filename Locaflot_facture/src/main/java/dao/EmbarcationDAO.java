package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import classes.Embarcation;

public class EmbarcationDAO extends DAO<Embarcation> {

	@Override
	public void create(Embarcation obj) {
		try {
			PreparedStatement prepare = this.connect.prepareStatement("INSERT INTO \"locaflot_facture\".embarcation VALUES(?, ?, ?, ?)");
			prepare.setString(1, obj.getId());
			prepare.setString(2, obj.getCouleur());
			prepare.setBoolean(3, obj.getDisponible());
			prepare.setString(4, obj.getType());
			prepare.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Embarcation read(int id) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Embarcation readBoat(String id) {
		Embarcation boat = new Embarcation();
		try {
			ResultSet result = this.connect
				.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)
				.executeQuery("SELECT * FROM \"locaflot_facture\".embarcation WHERE id = '" + id + "'");

			if (result.first())
				boat = new Embarcation(id, result.getString("couleur"), result.getBoolean("disponible"), result.getString("type"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return boat;
	}

	@Override
	public void update(Embarcation obj) {
		try {
			this.connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE).executeUpdate(
					"UPDATE \"locaflot_facture\".embarcation SET "
					+ "couleur='" + obj.getCouleur() + "', "
					+ "disponible='" + obj.getDisponible() + "', "
					+ "type= '" + obj.getType() + "'" 
					+ " WHERE code = '" + obj.getId() + "'"
					);
			obj = this.readBoat(obj.getId());
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void delete(Embarcation obj) {
		try {
			this.connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)
					.executeUpdate("DELETE FROM \"locaflot_facture\".embarcation WHERE code = '" + obj.getId() + "'");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<Embarcation> recupAll() {
		List<Embarcation> listeBoat = new ArrayList<Embarcation>();
		try {
			Statement requete = this.connect.createStatement();
			ResultSet curseur = requete.executeQuery("select * from \"locaflot_facture\".embarcation");

			while (curseur.next()) {
				Embarcation Boat = new Embarcation();
				
				Boat.setId(curseur.getString("id"));
				Boat.setCouleur(curseur.getString("couleur"));
				Boat.setDisponible(curseur.getBoolean("disponible"));
				Boat.setType(curseur.getString("type"));
				
				listeBoat.add(Boat);
			}
			curseur.close();
			requete.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listeBoat;
	}
	
	public int nb_embarcation(int id) {
		int nb=0;
		try {
			ResultSet result = this.connect
				.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)
				.executeQuery("SELECT \"locaflot_facture\".nb_embarcation(" + id + ");");

			if (result.first())
				nb = result.getInt("nb_embarcation");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return nb;
	}
	
}
