package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import classes.ContratLocation;
import classes.Embarcation;
import classes.Louer;

public class LouerDAO  extends DAO<Louer> {

	@Override
	public void create(Louer obj) {
		try {
			PreparedStatement prepare = this.connect.prepareStatement("INSERT INTO \"locaflot_facture\".louer VALUES(?, ?, ?)");
			prepare.setInt(1, obj.getId_contrat());
			prepare.setString(2, obj.getId_embarcation());
			prepare.setInt(3, obj.getNb_personnes());
			prepare.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Louer read(int id_contrat) {
		Louer location = new Louer();
		try {
			ResultSet result = this.connect
				.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)
				.executeQuery("SELECT * FROM \"locaflot_facture\".louer WHERE id_contrat = '" + id_contrat + "'");

			if (result.first())
				location = new Louer(id_contrat, result.getString("id_embarcation"), result.getInt("nb_personnes"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return location;
	}

	
	@Override
	public void update(Louer obj) {
		try {
			this.connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE).executeUpdate(
					"UPDATE \"locaflot_facture\".louer SET "
					+ "id_embarcation='" + obj.getId_embarcation() + "', "
					+ "nb_personnes='" + obj.getNb_personnes() + "', "
					+ " WHERE id_contrat = '" + obj.getId_contrat() + "'"
					);
			obj = this.read(obj.getId_contrat());
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void delete(Louer obj) {
		try {
			this.connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)
					.executeUpdate("DELETE FROM \"locaflot_facture\".louer WHERE id_contrat = '" + obj.getId_contrat() + "'");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	
	public List<String> recupBoat(int id) {
		List<String> listeBoat = new ArrayList<String>();
		String chaine;
		int time;
		float prix;

		// declaration de l'objet qui servira pour la requete SQL
		try {
			Statement requete = this.connect.createStatement();

			// definition de l'objet qui recupere le resultat de l'execution de la requete
			ResultSet curseur = requete.executeQuery("SELECT nom, type, prix_demi_heure, prix_heure, prix_demi_jour, prix_jour FROM \"locaflot_facture\".contrat_location " + 
					"INNER JOIN locaflot_facture.louer ON louer.id_contrat=contrat_location.id " + 
					"INNER JOIN locaflot_facture.embarcation ON embarcation.id=louer.id_embarcation " + 
					"INNER JOIN locaflot_facture.type_embarcation ON type_embarcation.code=embarcation.type " + 
					"WHERE contrat_location.id=" + id +";");
			
			Statement requete2 = this.connect.createStatement();

			// requete qui recupere le temps de location en minutes
			ResultSet curseur2 = requete2.executeQuery("SELECT \"locaflot_facture\".time_contrat(" + id + ");");
			curseur2.next();
			time=curseur2.getInt("time_contrat");
			
			// tant qu'il y a une ligne "resultat" a  lire
			while (curseur.next()) {
				if(time <= 30) {
		        	prix=curseur.getFloat("prix_demi_heure");
				}
		        else if (time <= 60) {
					prix=curseur.getFloat("prix_heure");
		        }
		        else if (time <= 300) {
		        	prix=curseur.getFloat("prix_demi_jour");
		        }
				else {
					prix=curseur.getFloat("prix_jour");
				}
				
				// objet pour la recup d'une ligne de la table Club
				chaine=curseur.getString("nom") + " - " + curseur.getString("type") + " - " + prix ;
				
				listeBoat.add(chaine);
				chaine="";
			}
			curseur.close();
			requete.close();
			curseur2.close();
			requete2.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listeBoat;
	}

	public String recupTime(int id) {
		String time="";
		try {
			Statement requete2 = this.connect.createStatement();
			ResultSet curseur2 = requete2.executeQuery("SELECT \"locaflot_facture\".time_contrat(" + id + ");");
			curseur2.next();
			time=curseur2.getString("time_contrat");
			//System.out.println("time:" + time + "\n");

			curseur2.close();
			requete2.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return time;
	}
	@Override
	public List<Louer> recupAll() {
		// TODO Auto-generated method stub
		return null;
	}
}
