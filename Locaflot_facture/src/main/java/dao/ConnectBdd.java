package dao;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Connection;

public class ConnectBdd {
	private static String url = "jdbc:postgresql://localhost:1999/c.lemeur";
	private static String user = "c.lemeur";
	private static String passwd = "P@ssword";
	private static Connection connect = null;

	public static Connection getInstance() {
		if (connect == null) {
			try {
				connect = DriverManager.getConnection(url, user, passwd);
				if (connect != null) {
		            System.out.println("Connected to the database!");
		        } else {
		            System.out.println("Failed to make connection!");
		        }
			} catch (SQLException e) {
		        System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
		    } catch (Exception e) {
		        e.printStackTrace();
		    }
		}
		return connect;
	}
}
