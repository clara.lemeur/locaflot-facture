package controleur;

import vues.Vue;

public class Main {

	public static void main(String[] args) {
        System.out.println( "Hello world!" );
        MainController leControleur = new MainController();
        Vue laVue = new Vue(leControleur);
		leControleur.setVue(laVue);
    }

}
