package controleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.*;

import classes.*;
import vues.*;
import dao.*;

@SuppressWarnings("unused")
public class MainController implements ActionListener {
	private Vue theview;
	
	private DAO<ContratLocation> gestionContract;
	private LouerDAO gestionLouer;
	private EmbarcationDAO gestionEmbarcation;
	private List<ContratLocation> contracts ;
	
	int index;
	
	/**
	 * Constructeur par default
	 */
	public MainController()
	{
		this.gestionContract = new ContratLocationDAO();
		this.gestionLouer = new LouerDAO();
		this.gestionEmbarcation = new EmbarcationDAO();
		this.contracts = gestionContract.recupAll();
	}
	
	
	

	@Override
	public void actionPerformed(ActionEvent e)
	{
		// si on clique sur Quitter
		if (e.getActionCommand().equals("quit")){
			Quitter();
		}
		// si on clique sur generate bill
		else if (e.getActionCommand().equals("Chose"))
		{
			//je voudrais afficher les info en dessous
			String [] contrat =theview.listeContract.getSelectedItem().toString().split(" - ");
			
			int id_contrat = Integer.parseInt(contrat[0]);
			//System.out.print(id_contrat);		

					
			theview.remplirTextArea(gestionLouer.recupBoat(id_contrat));
			theview.remplirInfoContract(id_contrat, gestionContract.read(id_contrat));
			theview.remplirTime(gestionLouer.recupTime(id_contrat));
			theview.remplirNbBoat(gestionEmbarcation.nb_embarcation(id_contrat));
		}
		else if (e.getActionCommand().equals("PDF"))
		{
			//je voudrais afficher les info en dessous
			String [] contrat =theview.listeContract.getSelectedItem().toString().split(" - ");
			
			int id_contrat = Integer.parseInt(contrat[0]);
			//System.out.print(id_contrat);		

			theview.genererPDF(id_contrat, gestionLouer.recupBoat(id_contrat));
		}
	}
	
	// methode appelee au lancement de l'application.
	public void setVue(Vue theview) {
		this.theview = theview;
		theview.remplirComboContract(contracts);
	}
	
	private void Quitter() {
		System.exit(0);
	}
}
