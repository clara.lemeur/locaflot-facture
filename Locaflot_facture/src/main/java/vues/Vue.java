package vues;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import classes.ContratLocation;
import controleur.MainController;

import java.awt.Color;
import java.awt.Font;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JTextArea;
import java.io.FileOutputStream;
import java.io.IOException;


public class Vue extends JFrame {

	/**
	 * Definition des elements de l'interface
	 */
	private static final long serialVersionUID = 1L;
	protected MainController controleur;
	private JPanel contentPane;
	public JComboBox<String> listeContract;
	public List<ContratLocation> contracts = new ArrayList<ContratLocation>();
	public JButton btnQuitter;
	public JLabel lblChoseContract;
	public JPanel Bills;
	public JLabel labelInfoContract;
	public JLabel labelStart;
	public JLabel labelEnd;
	public JLabel labelTime;
	public JLabel labelInfoBoat;
	public JTextArea txtNom;
	public JTextArea txtType; 
	public JTextArea txtPrix;
	public JLabel labelNbBoat;
	public JLabel labelTotal;
	public JButton btnChose;
	public JButton btnPDF;
	public JLabel lblOk;

	public boolean bool;



	/**
	 * Create the frame.
	 */
	public Vue(MainController controleur) {
		this.controleur = controleur;
		this.setVisible(true);
		setTitle("Generate Bills of contract");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 572, 499);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		btnPDF = new JButton("PDF");
		btnPDF.setBackground(Color.WHITE);
		btnPDF.setActionCommand("PDF");
		btnPDF.addActionListener(controleur);
		
		btnQuitter = new JButton("Quitter");
		btnQuitter.setBackground(Color.WHITE);
		btnQuitter.setActionCommand("quit");
		btnQuitter.addActionListener(controleur);
		
		btnChose = new JButton("Chose");
		btnChose.setBounds(180, 42, 123, 25);
		contentPane.add(btnChose);
		btnChose.setActionCommand("Chose");
		btnChose.addActionListener(controleur);
		btnChose.setVisible(true);
		btnQuitter.setBounds(191, 424, 118, 25);
		contentPane.add(btnQuitter);
		btnPDF.setBounds(191, 386, 118, 25);
		contentPane.add(btnPDF);
		btnPDF.setVisible(true);
		
		lblChoseContract = new JLabel("Chose a contract :");
		lblChoseContract.setBounds(63, 16, 118, 15);
		contentPane.add(lblChoseContract);
		lblChoseContract.setVisible(true);
		
		listeContract = new JComboBox<String>();
		listeContract.setActionCommand("rec");
		listeContract.addActionListener(controleur);
		listeContract.setBackground(Color.WHITE);
		listeContract.setBounds(180, 11, 266, 24);
		contentPane.add(listeContract);
		
		Bills = new JPanel();
		Bills.setBackground(Color.WHITE);
		Bills.setBounds(44, 93, 456, 255);
		contentPane.add(Bills);
		Bills.setLayout(null);
		
		labelInfoContract = new JLabel("Info about the contract : ");
		labelInfoContract.setFont(new Font("Yu Gothic UI", Font.PLAIN, 11));
		labelInfoContract.setBounds(25, 11, 126, 16);
		Bills.add(labelInfoContract);
		
		labelStart = new JLabel("Start : ");
		labelStart.setBounds(35, 38, 221, 16);
		Bills.add(labelStart);
		
		labelEnd = new JLabel("End : ");
		labelEnd.setBounds(284, 38, 214, 16);
		Bills.add(labelEnd);
		
		labelTime = new JLabel("Time : ");
		labelTime.setFont(new Font("Yu Gothic UI", Font.PLAIN, 11));
		labelTime.setBounds(45, 65, 126, 16);
		Bills.add(labelTime);
		
		labelInfoBoat = new JLabel("Info Boat : ");
		labelInfoBoat.setBounds(35, 92, 126, 16);
		Bills.add(labelInfoBoat);
		
		labelNbBoat = new JLabel("Number of Boats : ");
		labelNbBoat.setBounds(51, 228, 126, 16);
		Bills.add(labelNbBoat);
		
		labelTotal = new JLabel("Total : ");
		labelTotal.setBounds(314, 228, 126, 16);
		Bills.add(labelTotal);
		
		txtNom = new JTextArea();
		txtNom.setBounds(45, 112, 153, 105);
		Bills.add(txtNom);
		txtNom.setEditable(false);
		txtNom.setBackground(Color.WHITE);
		
		txtType = new JTextArea();
		txtType.setEditable(false);
		txtType.setBackground(Color.WHITE);
		txtType.setBounds(208, 112, 86, 105);
		Bills.add(txtType);
		
		txtPrix = new JTextArea();
		txtPrix.setEditable(false);
		txtPrix.setBackground(Color.WHITE);
		txtPrix.setBounds(304, 112, 97, 105);
		Bills.add(txtPrix);
		
		lblOk = new JLabel("");
		lblOk.setBounds(201, 359, 102, 14);
		contentPane.add(lblOk);
		
		labelTime.setVisible(true);
		labelEnd.setVisible(true);
		labelStart.setVisible(true);
		labelInfoBoat.setVisible(true);
		labelInfoContract.setVisible(true);
		labelNbBoat.setVisible(true);
		labelTotal.setVisible(true);

	}
	
	public void remplirComboContract(List<ContratLocation> contracts) {
		//labelInfoContract.setText("Info about the contract : ");
		listeContract.addItem("");
		for (ContratLocation c : contracts) {
			listeContract.addItem(c.getId() + " - " + c.getDate() + " - " + c.getHeure_debut() + " - " + c.getHeure_fin());
		}	
	}
	
	public void remplirTextArea(List<String> Boat) {
		//labelInfoBoat.setText("Info Boat : ");
		float total=0;
		String [] tab;
		txtNom.setText("Nom\n");
		txtType.setText("Type\n");
		txtPrix.setText("Prix\n");
		labelTotal.setText("Total : ");
		for (String e : Boat) {
			tab=e.split(" - ");
			//System.out.print("\n" + tab[0] + " " + tab[1] + " " + tab[2]);
			txtNom.setText(txtNom.getText() + tab[0] + "\n");
			txtType.setText(txtType.getText() + tab[1] + "\n");
			txtPrix.setText(txtPrix.getText() + tab[2] + "\n");
			total+=Float.parseFloat(tab[2]);
		}
		labelTotal.setText(labelTotal.getText() + total);
	}
	
	public void remplirInfoContract(int id, ContratLocation c) {
		labelStart.setText("Start : ");
		labelEnd.setText("End : ");
		labelStart.setText(labelStart.getText() + c.getDate() + " - " + c.getHeure_debut() );
		labelEnd.setText(labelEnd.getText() + c.getDate() + " - " + c.getHeure_debut() );
	}

	public void remplirTime(String time) {
		labelTime.setText("Time : ");
		labelTime.setText(labelTime.getText() + time + " min");
		
	}
	public void remplirNbBoat(int nb) {
		labelNbBoat.setText("Number of Boats : ");
		labelNbBoat.setText(labelNbBoat.getText() + nb );
	}
	
	public void genererPDF(int id_contrat, List<String> Boat) {
		try {
			Document doc = new Document();
			PdfWriter.getInstance(doc, new FileOutputStream("./pdf/Contract_bill_" + id_contrat + ".pdf"));
			// le texte que l'on veut mettre dans le fichier doit être « transformé » en objet Paragraphe (il existe d'autres types d'objets cf lien suivant) →
			
			String txt="";
			String [] tab;
			for (String e : Boat) {
				tab=e.split(" - ");
				txt=txt + aff(tab[0]) + aff(tab[1]) + aff(tab[2]) + "\n";
			}
			
			doc.open();
			Paragraph title = new Paragraph("Locaflot contract n°" + id_contrat + "\n\n\n");
			title.setAlignment(Element.ALIGN_CENTER);
			
			Paragraph info = new Paragraph(labelInfoContract.getText());
			Paragraph date = new Paragraph(labelStart.getText() + "\n" + labelEnd.getText());
			date.setIndentationLeft(50);
			Paragraph time = new Paragraph(labelTime.getText() + "\n");
			time.setIndentationLeft(50);
			
			Paragraph infoBoat = new Paragraph("\n" + labelInfoBoat.getText());
			Paragraph colonne = new Paragraph(aff("Nom") + aff("Type") + aff("Prix"));
			colonne.setIndentationLeft(50);
			Paragraph boat = new Paragraph(txt + "\n\n");
			boat.setIndentationLeft(50);
			Paragraph end = new Paragraph(labelNbBoat.getText() + "                       " + labelTotal.getText());
			end.setAlignment(Element.ALIGN_CENTER);
			
			doc.add(title);
			doc.add(info);
			doc.add(date);
			doc.add(time);
			doc.add(infoBoat);
			doc.add(colonne);
			doc.add(boat);
			doc.add(end);
			
			doc.close();
			System.out.println("PDF Generer");
			lblOk.setText("PDF Generate");
			}
		catch (DocumentException dE) {
		dE.printStackTrace();
		}
		catch (IOException ioE) {
		ioE.printStackTrace();
		}	
	}
	
	public String aff(String s) {
		int taille = 20;
		if(s.length()<taille) {
			for (int i=s.length();  i<taille; i++) {
				s=s + " ";
			}
		}
		return s;
	}
}
