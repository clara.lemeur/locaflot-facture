# Locaflot-Facture
Ce projet a pour but de fournir à l'organisation un
outils de calcul du montant total d'un contrat (facture).

## Licence
[AGPL v3](https://www.gnu.org/licenses/agpl-3.0.html)

## Cahier des Charges
Basé sur la version livrable
[0.3.1](https://framagit.org/sio/projets-minutes/projet-minute-locaflot-facture/-/tags)

## Principales dépendances
- postgresql version 42.2.12 pour accéder 
  à la base de donnée [PostgreSQL](https://www.postgresql.org/)
- [plantuml](https://plantuml.com/) pour la modélisation du diagramme
  de classe en UML
- [iText](https://mvnrepository.com/artifact/com.itextpdf/itext7-core/7.1.11) version 7.1.11 pour la
   génération de la facture en PDF
   
## Diagramme ULM des classes

![UML de locaflot_facture](doc/doc_UML_locaflot.png)


## Pour cloner le dépôt
```bash
git clone https://framagit.org/clara.lemeur/locaflot-facture.git
```
